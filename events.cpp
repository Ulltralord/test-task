#include <iostream>
#include <thread>
#include <chrono>
#include <random>
#include <vector>
#include <atomic>
#include <mutex>
#include <queue>
#include <csignal>
#include <cmath>


using namespace std;

static bool exit_flag = false; //флаг, позволяющий правильно очистить память при вызове SIGINT

// структура для хранения события
struct Event {
    chrono::system_clock::time_point timestamp;
    int random_number;
};

// генератор случайных чисел
int random_number_generator() {
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> dist(1, 1000000);
  return dist(gen);
}

// обработчик сигнала Ctrl+C
void sigint_handler(int signal) {
    exit_flag = true;
    // exit(0);
}

// генерация событий
void event_generator(int number_of_events, queue<Event>& event_queue, mutex& queue_mutex, atomic<int>& generators_count, mutex& output_mutex) {
    output_mutex.lock();
    cout << endl << "Starting new generator thread, UD = " << this_thread::get_id() << endl;
    output_mutex.unlock();
    for (int i = 0; i < number_of_events; i++) {
        if(exit_flag)
          return;
        Event event;
        event.timestamp = chrono::system_clock::now();
        event.random_number = random_number_generator();
        time_t time = chrono::system_clock::to_time_t(event.timestamp);
        output_mutex.lock();
        cout  << " Event " << i << ". " << " random_number: " << event.random_number << "   timestamp: " << ctime(&time);
        output_mutex.unlock();
        queue_mutex.lock();
        event_queue.push(event);
        generators_count++;
        queue_mutex.unlock();
    }
}

// обработка событий
void event_processor(queue<Event>& event_queue, atomic<int>& prime_numbers_count, mutex& queue_mutex,
                                    atomic<int>& generators_count, int numbers, mutex& output_mutex) {
  output_mutex.lock();
    cout << endl << "Starting new processor thread, UD = " << this_thread::get_id() << endl;
    output_mutex.unlock();
    while (true) {
      if(exit_flag)
        return;
        queue_mutex.lock();
        if (event_queue.empty() && generators_count == numbers) {// Проверяем, есть ли события в очереди  и отработали ли все генераторы
            queue_mutex.unlock();
            break;
        }
        if (!(event_queue.empty() && generators_count != numbers)){

          Event event = event_queue.front();
          event_queue.pop();
          queue_mutex.unlock();

          bool is_prime = true;
          if (event.random_number == 1 || event.random_number == 0)
              is_prime = false;
          else {
              for (int i = 2; i <= sqrt(event.random_number); i++) {
                  if (event.random_number % i == 0) {
                      is_prime = false;
                      break;
                  }
              }
          }

          if (is_prime){
            prime_numbers_count++;
          }

          output_mutex.lock();
          cout << " Number " << event.random_number << (is_prime ? " is prime" : " is not prime") << endl;
          output_mutex.unlock();
      }
      else
        queue_mutex.unlock();
    }
}


int main(int argc, char* argv[]) {
  if (argc != 4) {
      cout << "Usage: " << argv[0] << " <number_of_generators> <number_of_events> <number_of_processors>" << endl;
      exit(1);
  }

  int number_of_generators = stoi(argv[1]);
  int number_of_events = stoi(argv[2]);
  int number_of_processors = stoi(argv[3]);
  int numbers = number_of_generators * number_of_events;
  signal(SIGINT, sigint_handler);
  queue<Event> event_queue;
  mutex queue_mutex;
  mutex output_mutex;
  atomic<int> prime_numbers_count(0);
  atomic<int> generators_count(0);

  vector<thread> generators;  // создаем генераторы событий
  cout << "Creating " << number_of_generators << " event generators..." << endl;
  for (int i = 0; i < number_of_generators; i++)
      generators.push_back(thread(event_generator, number_of_events, ref(event_queue), ref(queue_mutex), ref(generators_count), ref(output_mutex)));

  vector<thread> processors;  // создаем обработчики событий
  cout << "Creating " << number_of_processors << " event processors..." << endl;
  for (int i = 0; i < number_of_processors; i++)
      processors.push_back(thread(event_processor, ref(event_queue), ref(prime_numbers_count), ref(queue_mutex), ref(generators_count), numbers, ref(output_mutex)));

  for (int i = 0; i < generators.size(); i++) { // ожидаем завершения всех генераторов
      generators[i].join();
  }
  generators.clear();

  for (int i = 0; i < processors.size(); i++) { // ожидаем завершения всех обработчиков
      processors[i].join();
  }
  processors.clear();

  if(exit_flag)
    cout << endl << "Got SIGINT signal. Exiting..." << endl;
  cout << endl << "Found " << prime_numbers_count << " prime numbers in " << generators_count << " events." << endl;

  queue<Event>().swap(event_queue);
  prime_numbers_count = 0;
  generators_count = 0;


  return 0;
}
